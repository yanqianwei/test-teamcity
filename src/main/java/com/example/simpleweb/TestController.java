package com.example.simpleweb;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

    @RequestMapping("/yqw")
    public String yqw(){
        return "尊敬的yqw，您好，您的开水已经为您准备好。";
    }
}
